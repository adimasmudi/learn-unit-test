﻿namespace TestLibrary
{

    public class Calculator
    {
        private readonly ILogCalc _logCalc;

        public Calculator(ILogCalc logCalc)
        {
            _logCalc = logCalc;
        }

        public int Add(int a, int b)
        {
            _logCalc.Log("Adding " + a + " and " + b);
            return a + b;
        }
    }
}