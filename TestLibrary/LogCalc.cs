﻿namespace TestLibrary
{

    public class LogCalc : ILogCalc
    {
        public bool Log(string message)
        {
            Console.WriteLine(message);
            return true;
        }
    }
}
