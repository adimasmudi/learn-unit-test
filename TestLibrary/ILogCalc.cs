﻿namespace TestLibrary
{

    public interface ILogCalc
    {
        bool Log(string message);
    }
}