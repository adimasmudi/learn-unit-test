﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TestLibrary;

namespace TestLibraryTests
{

    [TestClass]
    public class CalculatorShould
    {
        private readonly Mock<ILogCalc> _logCalcMock;

        public CalculatorShould()
        {
            _logCalcMock = new Mock<ILogCalc>();

            _logCalcMock.Setup(x => x.Log(It.IsAny<string>())).Returns(true);
        }

        [TestMethod]
        public void Add_Should_Add_Correctly_Two_Numbers()
        {
            // Arrange
            Calculator calculator = new Calculator(_logCalcMock.Object);
            int a = 15;
            int b = 20;

            // Act
            int result = calculator.Add(a, b);

            // Assert
            Assert.AreEqual(35, result);
        }
    }
}